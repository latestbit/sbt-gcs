package org.latestbit.gcsplugin

import sbt.PluginTrigger
import sbt.AutoPlugin

object GCSPlugin extends AutoPlugin {

  trait Keys {
    implicit def toSbtResolver(resolver: GCSResolver): sbt.Resolver = {
      new sbt.RawRepository(resolver, resolver.getName)
    }
  }

  object Keys extends Keys
  object autoImport extends Keys {
    val GCSResolver = org.latestbit.gcsplugin.GCSResolver
    val GCSPublisher = org.latestbit.gcsplugin.GCSPublisher
    val AccessRights = org.latestbit.gcsplugin.AccessRights
  }

  import sbt.Keys._

  override def projectSettings = Seq(
    publishMavenStyle := false
  )

  override def trigger: PluginTrigger = allRequirements
}
