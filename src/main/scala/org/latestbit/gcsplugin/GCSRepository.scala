package org.latestbit.gcsplugin

import java.util
import java.io._
import java.io.FileInputStream

import scala.collection.JavaConverters._

import com.google.cloud.storage.{ Storage, StorageOptions}
import com.google.cloud.storage.Bucket._
import org.latestbit.gcsplugin.AccessRights._
import org.apache.ivy.core.module.descriptor._
import org.apache.ivy.plugins.repository._

case class GCSRepository(bucketName: String, publishPolicy: AccessRigths) extends AbstractRepository {
  private val storage: Storage = StorageOptions.getDefaultInstance.getService

  override def getResource(source: String): GCSResource = {
    GCSResource.create(storage, bucketName, source)
  }

  override def get(source: String, destination: File): Unit = {

    val extSource = if (destination.toString.endsWith("sha1"))
      source + ".sha1"
    else if (destination.toString.endsWith("md5"))
      source + ".md5"
    else
      source

    GCSResource.toFile(storage, GCSResource.create(storage, bucketName, extSource), destination)
  }

  override def list(parent: String): util.List[String] = {
    storage.list(bucketName).getValues.asScala.map(_.getName).toList.asJava
  }

  override def put(artifact: Artifact, source: File, destination: String, overwrite: Boolean): Unit = {
    Option(storage.get(bucketName)) match {
      case Some(bucket) => {
        publishPolicy match {
          case AccessRights.PublicRead ⇒
            bucket.create(
              destination.replace("//", "/"),
              new FileInputStream(source),
              getContentType(artifact),
              BlobWriteOption.predefinedAcl(Storage.PredefinedAcl.PUBLIC_READ)
            )
          case AccessRights.InheritBucket ⇒
            bucket.create(
              destination.replace("//", "/"),
              new FileInputStream(source),
              getContentType(artifact)
            )
        }
      }
      case _ => {
        throw new IllegalStateException(s"Unable to open bucket: ${bucketName} in GCS")
      }
    }
  }

  private def getContentType(artifact: Artifact): String = {
    Option(artifact)
      .flatMap(a => Option(a.getType))
      .map(_.toLowerCase) 
      .collect {
        case "jar"  ⇒ "application/java-archive"
        case "xml"  ⇒ "application/xml"
        case "sha1" ⇒ "text/plain"
        case "md5"  ⇒ "text/plain"
        case "ivy"  ⇒ "application/xml"
      }
      .getOrElse("application/octet-stream")
  }
}
