name := "sbt-gcs"

description := "A SBT resolver and publisher for Google Cloud Storage"

version := "1.4.7"

organization := "org.latestbit"

homepage := Some(url("http://www.latestbit.com"))

licenses += ("Apache-2.0", url("https://www.apache.org/licenses/LICENSE-2.0.html"))

libraryDependencies ++= Vector(
	"org.scala-lang" % "scala-library" % scalaVersion.value,
	"org.scala-lang" % "scala-reflect" % scalaVersion.value,
	"org.scala-lang" % "scala-compiler" % scalaVersion.value,
	"org.apache.ivy" % "ivy" % "2.4.0",
	"com.google.cloud" % "google-cloud-storage" % "1.113.8"
)

sbtPlugin := true

crossSbtVersions := Vector("1.4.6")

scalaVersion := (CrossVersion partialVersion (sbtVersion in pluginCrossBuild).value match {
	case Some((1, _))  => "2.12.12"
	case _             => sys error s"Unhandled sbt version ${(sbtVersion in pluginCrossBuild).value}"
})

publishMavenStyle := true

publishTo := {
	val nexus = "https://oss.sonatype.org/"
	if (isSnapshot.value)
		Some("snapshots" at nexus + "content/repositories/snapshots")
	else
		Some("releases" at nexus + "service/local/staging/deploy/maven2")
}

pomExtra := (
	<scm>
		<url>https://bitbucket.org/latestbit/sbt-gcs</url>
		<connection>scm:git:https://bitbucket.org/latestbit/sbt-gcs</connection>
		<developerConnection>scm:git:https://bitbucket.org/latestbit/sbt-gcs</developerConnection>
	</scm>
	<developers>
		<developer>
			<id>abdulla</id>
			<name>Abdulla Abdurakhmanov</name>
			<url>http://www.latestbit.com</url>
		</developer>
	</developers>
)


scalacOptions ++= Seq(
	"-encoding", "UTF-8",
	"-Xlog-reflective-calls",
	"-Xlint",
	"-deprecation",
	"-feature",
	"-language:_",
	"-unchecked"
)
